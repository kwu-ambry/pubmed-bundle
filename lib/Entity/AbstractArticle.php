<?php
namespace Ambry\PubMedBundle\Entity;

use Ambry\PubMedBundle\Entity\Interfaces\PubMedArticleInterface;

abstract class AbstractArticle implements PubMedArticleInterface
{
    protected $xml;

    protected $pmid;

    /**
     * Magic Method
     * @return string return object print_r for debugging
     */
    public function __toString()
    {
        return print_r($this->xml, true);
    }

    /**
     * Get JSON result of all items
     * @return string JSON encoded string of results
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Loop through authors, return Lastname First Initial
     * @return array The list of authors
     */
    public function getAuthors()
    {
        $authors = array();
        if (isset($this->xml->AuthorList)) {
            try {
                foreach ($this->xml->AuthorList->Author as $author) {
                    if(!isset($author->LastName) && !isset($author->Initials) && isset($author->CollectiveName)) {
                        $authors [] = (string) $author->CollectiveName;
                    } else {
                        $authors[] = (string) $author->LastName . ' ' . (string) $author->Initials;
                    }

                }
            } catch (\Exception $e) {
                $a = $this->xml->AuthorList->Author;
                $authors[] = (string) $a->LastName .' '. (string) $a->Initials;
            }
        }

        return $authors;
    }

    /**
     * Run all getters on the xml object
     * @return array array of all getters
     */
    public function toArray()
    {
        return array(
            'PMID'         => $this->getPubMedId(),
            'Volume'       => $this->getVolume(),
            'Issue'        => $this->getIssue(),
            'PubYear'      => $this->getPubYear(),
            'PubMonth'     => $this->getPubMonth(),
            'ISSN'         => $this->getISSN(),
            'JournalTitle' => $this->getJournalTitle(),
            'JournalAbbr'  => $this->getJournalAbbr(),
            'Pagination'   => $this->getPagination(),
            'ArticleTitle' => $this->getArticleTitle(),
            'AbstractText' => $this->getAbstractText(),
            'Affiliation'  => $this->getAffiliation(),
            'Authors'      => $this->getAuthors()
        );
    }

    /**
     * Return array of all results
     * @return array array of results
     */
    public function getResult()
    {
        return $this->toArray();
    }

    public function getPubMedId()
    {
        return $this->pmid;
    }
}