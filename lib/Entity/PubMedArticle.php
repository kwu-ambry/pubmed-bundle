<?php

/**
 * PHP wrapper for NCBI PubMed
 *   Extend Pubmed for term specific searching
 *
 * @author  Tom Ploskina <tploskina@ambrygen.com>
 * @copyright Copyright (c) 2013 Ambry Genetics http://www.ambrygen.com
 * @license MIT http://opensource.org/licenses/MIT
 * @version 1.0
 */

namespace Ambry\PubMedBundle\Entity;

use SimpleXMLElement;

class PubMedArticle extends AbstractArticle
{

  /**
   * Additional info for journal abbreviation
   *
   * @var SimpleXMLElement
   */
  private $medline_journal_info;

  /**
   * Constructor, init
   * @param SimpleXMLElement $xml The main xml object to work on
   */
  public function __construct(SimpleXMLElement $xml)
  {
    $this->xml = $xml->PubmedArticle->MedlineCitation->Article;
    $this->pmid = (string) $xml->PubmedArticle->MedlineCitation->PMID;
    $this->medline_journal_info = $xml->PubmedArticle->MedlineCitation->MedlineJournalInfo;
  }

  /**
   * Magic Method
   * @return string return object print_r for debugging
   */
  public function __toString()
  {
    return print_r($this->xml, true);
  }

  /**
   * Get JSON result of all items
   * @return string JSON encoded string of results
   */
  public function toJson()
  {
    return json_encode($this->toArray());
  }

  /**
   * Run all getters on the xml object
   * @return array array of all getters
   */
  public function toArray()
  {
    return array(
      'PMID'         => $this->getPubMedId(),
      'PubYear'      => $this->getPubYear(),
      'PubMonth'     => $this->getPubMonth(),
      'JournalTitle' => $this->getJournalTitle(),
      'JournalAbbr'  => $this->getJournalAbbr(),
      'Pagination'   => $this->getPagination(),
      'ArticleTitle' => $this->getArticleTitle(),
      'AbstractText' => $this->getAbstractText(),
      'Affiliation'  => $this->getAffiliation(),
      'Authors'      => $this->getAuthors()
    );
  }

  /**
   * Return array of all results
   * @return array array of results
   */
  public function getResult()
  {
    return $this->toArray();
  }

  /**
   * Loop through authors, return Lastname First Initial
   * @return array The list of authors
   */
  public function getAuthors()
  {
    $authors = array();
    if (isset($this->xml->AuthorList)) {
      try {
        foreach ($this->xml->AuthorList->Author as $author) {
          if(!isset($author->LastName) && !isset($author->Initials) && isset($author->CollectiveName)) {
            $authors [] = (string) $author->CollectiveName;
          } else {
            $authors[] = (string) $author->LastName . ' ' . (string) $author->Initials;
          }

        }
      } catch (Exception $e) {
        $a = $this->xml->AuthorList->Author;
        $authors[] = (string) $a->LastName .' '. (string) $a->Initials;
      }
    }

    return $authors;
  }

  public function getPubMedId()
  {
    return $this->pmid;
  }

  /**
   * Get the volume from the SimpleXMLElement
   * @return string Journal Volume Number
   */
  public function getJournalVolume()
  {
    return (string) $this->xml->Journal->JournalIssue->Volume;
  }

  /**
   * Get the JournalIssue from the SimpleXMLElement
   * @return string JournalIssue
   */
  public function getJournalIssue()
  {
    return (string) $this->xml->Journal->JournalIssue->Issue;
  }

  /**
   * Get the PubYear from the SimpleXMLElement
   * @return string PubYear
   */
  public function getPubYear()
  {

      //have to use isset as ->Year never null, its empty SimpleXMLElement object
      if(isset($this->xml->Journal->JournalIssue->PubDate->Year)){
          return (string) $this->xml->Journal->JournalIssue->PubDate->Year;
      }

      if(isset($this->xml->Journal->JournalIssue->PubDate->MedlineDate)){
          return explode(' ',$this->xml->Journal->JournalIssue->PubDate->MedlineDate)[0] ?? null;
      }
    return null;
  }

  /**
   * Get the PubMonth from the SimpleXMLElement
   * @return string PubMonth
   */
  public function getPubMonth()
  {
    return (string) $this->xml->Journal->JournalIssue->PubDate->Month;
  }

  /**
   * Get the ISSN from the SimpleXMLElement
   * @return string Journal ISSN
   */
  public function getJournalISSN()
  {
    return (string) $this->xml->Journal->ISSN;
  }

  /**
   * Get the Journal Title from the SimpleXMLElement
   * @return string Journal Title
   */
  public function getJournalTitle()
  {
    return (string) $this->xml->Journal->Title;
  }

  /**
   * Get the ISOAbbreviation from the SimpleXMLElement
   * @return string ISOAbbreviation
   */
  public function getJournalAbbr()
  {
    return (string) ($this->xml->Journal->ISOAbbreviation ?? $this->medline_journal_info->MedlineTA);
  }

  /**
   * Get the Pagination from the SimpleXMLElement
   * @return string Pagination
   */
  public function getPagination()
  {
    return (string) $this->xml->Pagination->MedlinePgn;
  }

  /**
   * Get the ArticleTitle from the SimpleXMLElement
   * @return string ArticleTitle
   */
  public function getArticleTitle()
  {
      //Some titles contain inner html with gene names. This strips the html but keeps the gene names.
    if(!empty($this->xml->ArticleTitle->children())) {
        $doc = new \DOMDocument();
        $doc->loadXML($this->xml->ArticleTitle->asXML());
        return (string)$doc->firstChild->nodeValue;
    }
    return (string) $this->xml->ArticleTitle;
  }

  /**
   * Get the AbstractText from the SimpleXMLElement
   * @return string AbstractText
   */
  public function getAbstractText()
  {
    return (string) $this->xml->Abstract->AbstractText;
  }

  /**
   * Get the Affiliation from the SimpleXMLElement
   * @return string Affiliation
   */
  public function getAffiliation()
  {
    return (string) $this->xml->Affiliation;
  }
}
