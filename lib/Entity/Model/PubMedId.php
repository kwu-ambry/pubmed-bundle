<?php

/**
 * PHP wrapper for NCBI PubMed
 *   Extend Pubmed for term specific searching
 *
 * @author    Tom Ploskina <tploskina@ambrygen.com>
 * @copyright Copyright (c) 2013 Ambry Genetics http://www.ambrygen.com
 * @license   MIT http://opensource.org/licenses/MIT
 * @version   1.0
 */

namespace Ambry\PubMedBundle\Entity\Model;

use Ambry\PubMedBundle\Entity\GeneReviewArticle;
use Ambry\PubMedBundle\Entity\Interfaces\PubMedArticleInterface;
use Ambry\PubMedBundle\Entity\PubMed;
use Ambry\PubMedBundle\Entity\PubMedArticle;
use SimpleXMLElement;

class PubMedId extends PubMed
{
    /**
     * @var PubMedArticleInterface
     */
    private $article = null;

    /**
     * @param PubMedArticleInterface $article
     *
     * @return $this
     */
    public function setArticle(PubMedArticleInterface $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return PubMedArticleInterface
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Return the URL of the single PMID fetch
     * @return string URL of NCBI single article fetch
     */
    protected function getUrl()
    {
        return 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi';
    }

    /**
     * Specific to Single mode searches
     * @return string parameter passed in the URI, eg, "&id=23234234"
     */
    protected function getSearchName()
    {
        return 'id';
    }

    /**
     * Main function of this class, get the result xml, searching
     * by PubMedId (PMID)
     *
     * @param  string $pmid PubMedID
     *
     * @return self
     * @throws \Exception
     */
    public function query($pmid)
    {
        $content = $this->sendRequest($pmid);
        $xml = new SimpleXMLElement($content);

        if (count($xml->PubmedArticle) === 1 || count($xml->PubmedBookArticle) === 1) {
            $this->articleCount = 1;
            $this->setArticle(count($xml->PubmedArticle) === 1 ? new PubMedArticle($xml) : new GeneReviewArticle($xml));
        }

        return $this;
    }
}