<?php


namespace Ambry\PubMedBundle\Entity\Interfaces;


interface PubMedArticleInterface {

    /**
     * Magic Method
     * @return string return object print_r for debugging
     */
    public function __toString();

    /**
     * Get JSON result of all items
     * @return string JSON encoded string of results
     */
    public function toJson();

    /**
     * Run all getters on the xml object
     * @return array array of all getters
     */
    public function toArray();

    /**
     * Return array of all results
     * @return array array of results
     */
    public function getResult();

    /**
     * Loop through authors, return Lastname First Initial
     * @return array The list of authors
     */
    public function getAuthors();

    /**
     * Get the PubMed ID from the SimpleXMLElement
     * @return mixed
     */
    public function getPubMedId();

    /**
     * Get the volume from the SimpleXMLElement
     * @return string Journal Volume Number
     */
    public function getJournalVolume();

    /**
     * Get the JournalIssue from the SimpleXMLElement
     * @return string JournalIssue
     */
    public function getJournalIssue();

    /**
     * Get the PubYear from the SimpleXMLElement
     * @return string PubYear
     */
    public function getPubYear();

    /**
     * Get the PubMonth from the SimpleXMLElement
     * @return string PubMonth
     */
    public function getPubMonth();

    /**
     * Get the ISSN from the SimpleXMLElement
     * @return string Journal ISSN
     */
    public function getJournalISSN();

    /**
     * Get the Journal Title from the SimpleXMLElement
     * @return string Journal Title
     */
    public function getJournalTitle();

    /**
     * Get the ISOAbbreviation from the SimpleXMLElement
     * @return string ISOAbbreviation
     */
    public function getJournalAbbr();

    /**
     * Get the Pagination from the SimpleXMLElement
     * @return string Pagination
     */
    public function getPagination();

    /**
     * Get the ArticleTitle from the SimpleXMLElement
     * @return string ArticleTitle
     */
    public function getArticleTitle();

    /**
     * Get the AbstractText from the SimpleXMLElement
     * @return string AbstractText
     */
    public function getAbstractText();

    /**
     * Get the Affiliation from the SimpleXMLElement
     * @return string Affiliation
     */
    public function getAffiliation();
}