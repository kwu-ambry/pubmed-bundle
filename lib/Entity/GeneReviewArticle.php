<?php
namespace Ambry\PubMedBundle\Entity;

use Ambry\PubMedBundle\Entity\Interfaces\PubMedArticleInterface;

class GeneReviewArticle extends AbstractArticle implements PubMedArticleInterface
{
    /**
     * Constructor, init
     * @param \SimpleXMLElement $xml The main xml object to work on
     */
    public function __construct(\SimpleXMLElement $xml)
    {
        $this->xml = $xml->PubmedBookArticle->BookDocument;
        $this->pmid = (string) $this->xml->PMID;
    }

    public function getJournalTitle()
    {
        return $this->nodeHtml($this->xml->Book->BookTitle);
    }
    public function getArticleTitle()
    {
        return $this->nodeHtml($this->xml->ArticleTitle);
    }

    public function getAbstractText()
    {
        $out = [];
        foreach($this->xml->Abstract->AbstractText as $text) {
            $out[]= $this->nodeHtml($text);
        }
        return implode('', $out);
    }

    public function getPubYear()
    {
        return (string) $this->xml->Book->PubDate->Year;
    }

    public function getJournalVolume()
    {
        return 'N/A';
    }

    public function getJournalIssue()
    {
        return 'N/A';
    }

    public function getPubMonth()
    {
        return 'N/A';
    }

    public function getJournalISSN()
    {
        return 'N/A';
    }

    public function getJournalAbbr()
    {
        return 'N/A';
    }

    public function getPagination()
    {
        return 'N/A';
    }

    public function getAffiliation()
    {
        return 'N/A';
    }

    /**
     * SimpleXml views html as other xml elements. Casting as a string removes children html.
     * @param $text
     * @return string
     */
    private function nodeHtml($text)
    {
        return strip_tags($text->asXml());
    }

}